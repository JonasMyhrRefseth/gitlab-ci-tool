# GitLab CI Tool

Based on a .gitlab-ci.yml file, you can pick a job and this tool will generate a new .gitlab-ci.yml file that contains everything needed for that job to finish (Based on dependencies etc).