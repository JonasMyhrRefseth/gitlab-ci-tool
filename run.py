#!/usr/bin/env python3

import yaml
import json
import argparse
from collections import OrderedDict

def represent_dictionary_order(self, dict_data):
    return self.represent_mapping('tag:yaml.org,2002:map', dict_data.items())

def setup_yaml():
    yaml.add_representer(OrderedDict, represent_dictionary_order)  

def handle_job(job, data, new_data):
    if "stage" in data[job]:
        if not (data[job]["stage"] in new_data["stages"]):
            new_data["stages"].insert(0, data[job]["stage"])
    if "extends" in data[job]:
        if not (data[job]["extends"] in new_data):
            data, new_data = handle_job(data[job]["extends"], data, new_data)
    if "dependencies" in data[job]:
        for dep in data[job]["dependencies"]:
            if not dep in new_data:
                data, new_data = handle_job(dep, data, new_data)
    if "needs" in data[job]:
        for dep in data[job]["needs"]:
            if not dep in new_data:
                data, new_data = handle_job(dep, data, new_data)
    new_data[job] = data[job]
    return (data, new_data)

def generate(file, keep_jobs):
    # Set the yaml parser up so it keeps the order of the jobs the same
    setup_yaml()
    new_data = OrderedDict()
    with open(file, 'r') as stream:
        try:
            data = yaml.safe_load(stream)
            for job in data:
                if job == "stages":
                    # Looks like we have some stages.
                    # We will handle this when parsing the jobs,
                    # so we just end up with the once we need.
                    new_data[job] = []
                elif job in keep_jobs:
                    data, new_data = handle_job(job, data, new_data)
            
            # No need for a stages array if it is empty.
            if new_data["stages"] == []:
                del new_data["stages"]

            # We want to sort the stages based on the original list
            if new_data["stages"]:
                ordered_stages = []
                for stage in data["stages"]:
                    if stage in new_data["stages"]:
                        ordered_stages.append(stage)
                new_data["stages"] = ordered_stages
            
        except yaml.YAMLError as exc:
            print(exc)
    return yaml.dump(new_data)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--yaml", help="Original yaml file.", type=str)
    parser.add_argument("--jobs", help="Specify the job(s) to keep.", type=str, nargs='+')
    parser.add_argument("--output", help="Store the output in a file.", type=str)
    args = parser.parse_args()
    yaml = generate(args.yaml, args.jobs)
    if args.output:
        with open(args.output, 'w') as output:
            output.write(yaml)
        print(f"New yaml stored in {args.output}")
    else:
        print(yaml)
